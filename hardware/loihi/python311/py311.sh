mkdir -p $HOME/practical
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $HOME/practical/miniconda.sh
bash $HOME/practical/miniconda.sh -b -p $HOME/practical/miniconda
rm $HOME/practical/miniconda.sh
source "$HOME/nengoloihi/miniconda/etc/profile.d/conda.sh"
conda config --set always_yes yes --set changeps1 no
conda update -n base -c defaults conda
conda create -n practical python=3.11 -c intel
conda activate practical
conda install -n practical -c intel numpy scipy
conda install -n practical -c conda-forge lava --freeze-installed

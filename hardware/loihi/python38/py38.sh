mkdir -p $HOME/nengoloihi
wget https://www.nengo.ai/nengo-examples/loihi/communication-channel.ipynb -O $HOME/nengoloihi/communication-channel.ipynb
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O $HOME/nengoloihi/miniconda.sh
bash $HOME/nengoloihi/miniconda.sh -b -p $HOME/nengoloihi/miniconda
rm $HOME/nengoloihi/miniconda.sh
source "$HOME/nengoloihi/miniconda/etc/profile.d/conda.sh"
conda config --set always_yes yes --set changeps1 no
conda update -n base -c defaults conda
wget https://gitlab.socsci.ru.nl/snnsimulator/nmcsetup/-/raw/master/python38/environment.yml -O $HOME/nengoloihi/environment.yml
conda env create -f $HOME/nengoloihi/environment.yml
rm $HOME/nengoloihi/environment.yml

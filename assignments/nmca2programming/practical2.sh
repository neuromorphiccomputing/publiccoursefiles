#!/bin/zsh

mkdir -p $(pwd)/practical2
wget https://github.com/nielsvharten/SNN-builder/releases/download/1.0.0/SNN-builder-v1.0.0-build.zip -O $(pwd)/practical2/gui.zip
unzip $(pwd)/practical2/gui.zip -d $(pwd)/practical2
mv $(pwd)/practical2/SNN-builder-v1.0.0-build $(pwd)/practical2/gui
conda env create -f $(pwd)/practical2/gui/environment.yml
